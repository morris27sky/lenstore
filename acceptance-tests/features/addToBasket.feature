@add_to_bsket
Feature: Add to basket
    As a use
    I want go to the Dailies AquaComfort Plus page and select my contact lens prescription
    So that I can add to basket and proceed to the checkout

    @US2_AC1
    Scenario: Verify product page links to the correct page and that the page is displayed correctly
        Given I navigate to "https://www.lenstore.co.uk/"
        When I select Dailies AquaComfort Plus from the product list
        Then I should be on the Dailies AquaComfort Plus page

    @US2_AC2
    Scenario: Verify that the product cannot be added to the basket without selecting the prescription
        Given I navigate to "https://www.lenstore.co.uk/"
        And I select Dailies AquaComfort Plus from the product list
        When I select add to basket
        Then I should see an error message

    @US2_AC3
    Scenario: Once the product has been added to the basket the checkout is displaying the product
        Given I navigate to "https://www.lenstore.co.uk/"
        And I select Dailies AquaComfort Plus from the product list
        And I set the prescription for the left eye to "9559"
        And I set the prescription for the right eye to "9559"
        When I select add to basket
        Then the basket should contain the correct selected product