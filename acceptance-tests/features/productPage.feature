@product_page
Feature: Product page
    As a user
    I want to click the Dailies AquaComfort Plus item
    So that we can see that the lens selection page is displayed

    @US1_AC1 @AC2
    Scenario: Verify product page links to the correct page and that the page is displayed correctly
        Given I navigate to "https://www.lenstore.co.uk/"
        When I select Dailies AquaComfort Plus from the product list
        Then I should be on the Dailies AquaComfort Plus page

    @US1_AC3
    Scenario: Verify add to basket button is displayed
        Given I navigate to "https://www.lenstore.co.uk/"
        When I select Dailies AquaComfort Plus from the product list
        Then the add to basket button should be displayed