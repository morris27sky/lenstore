module.exports = {
  create: () => {
    const maxRetries = 3;

    browser.addCommand('waitForElementVisible', (selector, retries = 0) => {
      try {
        browser.waitForVisible(selector);
      } catch (err) {
        if (retries < maxRetries) {
          browser.waitForElementVisible(selector, (retries += 1));
        }
        else
        { console.log(err);}
      }
    });

    browser.addCommand('waitAndClickElement', (selector, retries = 0) => {
      browser.waitForElementVisible(selector);

      try {
        browser.click(selector);
      } catch (err) {
        if (retries < maxRetries) {
          browser.waitAndClickElement(selector, (retries += 1));
        }
      }
    });

    browser.addCommand('waitAndGetElementText', (textSelector, retries = 0) => {
      try {
        browser.waitForElementVisible(textSelector);
        const elementText = browser.getText(textSelector);
        return elementText;
      } catch (err) {
        if (retries < maxRetries) {
          browser.waitAndGetElementText(textSelector, (retries += 1));
        }
      }
    });

    browser.addCommand('getCurrentUrl', () => {
      try {
        return browser.getUrl();

      } catch (err) {
        console.error(err)
      }
    });

    browser.addCommand('isElementVisible', selector => {
      try {
        if (browser.isVisible(selector)) {
          return true;
        }
        return false;
      } catch (err) {
      }
    });

    browser.addCommand('getPageTitle', () => {
      try {
        return browser.getTitle();

      } catch (err) {
        console.error(err)
      }
    });

  }
};