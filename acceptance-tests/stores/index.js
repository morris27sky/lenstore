const state = {
  state: {
    productPrice: undefined
  },

  set productPrice(value) {
    this.state.productPrice = value
  },

  get productPrice() {
    return this.state.productPrice
  }
}

module.exports = state