class ProductPage {

  get addToBasket() {
    return $('#addtobasket_button').selector;
  }

  get purchaseBoxProduct() {
    return $('#purchaseBoxProduct').selector;
  }
  
  get leftEyePowerDropdown() {
    return $('select#option1').selector;
  }

  get rightEyePowerDropdown() {
    return $('select#optionB1').selector;
  }

  get undiscountedPrice() {
    return $('.undiscountedPrice').selector;
  }

  setLeftEyePrescrition(power) {
    browser.selectByValue(this.leftEyePowerDropdown, power);
  }

  setRightEyePrescrition(power) {
    browser.selectByValue(this.rightEyePowerDropdown, power);
  }

};

module.exports = new ProductPage();
