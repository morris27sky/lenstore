class HomePage {

  get dailiesAquaComfortPlus() {
    return $$('[title="Dailies AquaComfort Plus"]');
  }
  
  open(path) {
    browser.url(path);
  }

};

module.exports = new HomePage();
