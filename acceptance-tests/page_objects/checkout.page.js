class CheckoutPage {

  get checkoutProductTitle() {
    return $(".basketTableRows.js-basket-table-row .cartTableTitle").selector;
  }

  get checkoutProductTitleItemPrice() {
    return $(".basketTableRows.js-basket-table-row .border.unitPriceCol").selector;
  }  

};

module.exports = new CheckoutPage();
