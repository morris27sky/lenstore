const { Given } = require('cucumber');

Given(/^I navigate to "(.*)"$/, url => {
  browser.url(url);
});