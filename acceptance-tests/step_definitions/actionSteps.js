const { When } = require('cucumber');
const homepage = require('../page_objects/home.page');
const productPage = require('../page_objects/product.page');
const { state }  = require('../stores/index');

When(/^I select Dailies AquaComfort Plus from the product list$/, () => {
    const element = homepage.dailiesAquaComfortPlus[0];
    browser.waitAndClickElement(element.selector)
});

When(/^I select add to basket$/, () => {
    state.productPrice = browser.waitAndGetElementText(productPage.undiscountedPrice);
    browser.waitAndClickElement(productPage.addToBasket)
});

When(/^I set the prescription for the left eye to "(.*)"$/, (power) => {
    productPage.setLeftEyePrescrition(power)
});

When(/^I set the prescription for the right eye to "(.*)"$/, (power) => {
    productPage.setRightEyePrescrition(power)
});