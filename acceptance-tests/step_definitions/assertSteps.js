const { Then } = require('cucumber');
const productPage = require('../page_objects/product.page');
const checkoutPage = require('../page_objects/checkout.page');
const { state }  = require('../stores/index');

Then(/^I should be on the Dailies AquaComfort Plus page$/, () => {
    expect(browser.getCurrentUrl()).to.include("daily-disposables/dailies-aquacomfort-plus_p5");
    expect(browser.getPageTitle()).to.include("Dailies AquaComfort Plus Lenses, Buy Online Today");
    expect(browser.isElementVisible(productPage.addToBasket)).to.be.true;
});

Then(/^the add to basket button should be displayed$/, () => {
    expect(browser.isElementVisible(productPage.addToBasket)).to.be.true;
});

Then(/^I should see an error message$/, () => {
    const msg1 = "Right Eye: Please complete the empty options on the left."
    const msg2 = "Left Eye: Please complete the empty options on the left."
    expect(browser.waitAndGetElementText(productPage.purchaseBoxProduct)).to.include(msg1 || msg2);
});

Then(/^the basket should contain the correct selected product$/, () => {
    expect(browser.waitAndGetElementText(checkoutPage.checkoutProductTitle)).to.include("Dailies AquaComfort Plus");
    expect(browser.waitAndGetElementText(checkoutPage.checkoutProductTitle)).to.include("-15.00");
    expect(browser.waitAndGetElementText(checkoutPage.checkoutProductTitleItemPrice)).to.include(state.productPrice)
});