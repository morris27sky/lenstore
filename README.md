## Tech
* [Selenium WebdriverIO](http://webdriver.io/) - Node.js Selenium implementation for writing functional tests
* [Chai](http://chaijs.com/api/bdd/) - Assertion library for verifying the results

## Assumptions
Goole chrome is required and has been downloaded.

## Getting Started
After cloning the ropo, go ahead and install package dependencies.

```sh
$ cd lenstore
$ npm i
```

Then to run the tests
```sh
$ npm run test
```

Tests are run headless by default. To run without headless, navigate to the config (config/wdio-conf.js) and remove the '--headless' argument.

reports will be generated and saved in acceptance-tests/report folder